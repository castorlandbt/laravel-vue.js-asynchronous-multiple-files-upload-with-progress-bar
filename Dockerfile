FROM php:7.4-apache

#Install necessary packages
RUN apt-get update \
    && apt-get install -y git libpng-dev \
    wget \
    nano \
    cron \
    zip \
    libzip-dev libicu-dev g++ \
    libwebp-dev \
    libjpeg62-turbo-dev \
    libxpm-dev \
    libfreetype6-dev \
    libmagickwand-dev \
    imagemagick libmagickcore-dev \
    libmemcached-dev \
    && pecl install imagick \
    && pecl install memcached \
    && apt-get clean && apt-get autoremove && rm -rf /var/lib/apt/lists/*
RUN docker-php-ext-configure intl && docker-php-ext-configure zip && docker-php-ext-configure gd
RUN docker-php-ext-install pdo pdo_mysql gd mysqli intl zip
RUN docker-php-ext-enable imagick \
    memcached \
    opcache

# Install APCu and APC backward compatibility
#RUN pecl install apcu \
#    && pecl install apcu_bc-1.0.3 \
#    && docker-php-ext-enable apcu --ini-name 10-docker-php-ext-apcu.ini \
#    && docker-php-ext-enable apc --ini-name 20-docker-php-ext-apc.ini

# Install PHPUnit
#RUN wget https://phar.phpunit.de/phpunit.phar -O /usr/local/bin/phpunit \
#    && chmod +x /usr/local/bin/phpunit

# Set up Apache
RUN a2enmod rewrite \
    && sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf \
    && sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf \
    && echo "ServerName laravel-image.test" >> /etc/apache2/apache2.conf \
    && sed -i -e 's/#ServerName www.example.com/ServerName laravel-image.test/g' /etc/apache2/sites-available/*.conf

# Use the default production configuration
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

#Install Composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php --install-dir=. --filename=composer \
    && mv composer /usr/local/bin/

# Cron
# ADD .cron /etc/cron.d/cron
# RUN chmod 0644 /etc/cron.d/cron \
#    && touch /var/log/cron.log \
#    && service cron start
