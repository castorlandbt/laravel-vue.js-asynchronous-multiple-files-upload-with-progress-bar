<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

- Two different ways how to upload file in laravel
- Validating the file & save it into database
- Display uploaded files
- Beautify standard file input without any plugin
- Asyncronous multiple files upload
- Showing progress bar while files being uploaded

# Source
https://www.youtube.com/watch?v=sJuG32d8_uA&list=WL&index=2&t=1057s

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
